FROM openjdk:8-jdk-alpine
EXPOSE 8081
ADD /target/springbootdemo-0.0.1-SNAPSHOT.jar app-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","app-0.0.1-SNAPSHOT.jar"]