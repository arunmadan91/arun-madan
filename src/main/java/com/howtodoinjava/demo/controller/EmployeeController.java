package com.howtodoinjava.demo.controller;

import java.util.ArrayList;
import java.util.List;

import io.sentry.Sentry;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.howtodoinjava.demo.model.Employee;

@RestController
public class EmployeeController {

    @RequestMapping("/")
    public List<Employee> getEmployees() {
        List<Employee> employeesList = new ArrayList<Employee>();
        try {
            employeesList.add(new Employee(1, "lokesh", "gupta", "howtodoinjava@gmail.com"));
            throw new Exception("This is a test.");
        } catch (Exception e) {
            Sentry.captureException(e);
        }
        return employeesList;
    }
}
